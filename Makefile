PREFIX = /usr/local

CC = gcc
CFLAGS = -lpci

all: fetchy

fetchy: fetchy.c
	$(CC) fetchy.c -o $@ $(CFLAGS) 

clean:
	-rm -f fetchy

install: all
	install -Dm755 fetchy $(DESTDIR)$(PREFIX)/bin/fetchy

uninstall:
	-rm -f $(DESTDIR)$(PREFIX)/bin/fetchy

.PHONY: all install uninstall clean
